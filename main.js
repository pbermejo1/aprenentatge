$(document).ready(function(){
  console.log("Welcome to jQuery.");


  $(".preguntas > .respuesta").on("click", function() {

      let hanClicado = false;

      // Miramos todos los hermanos de la pregunta
      $(this).siblings().each(function () {
        if ($(this).hasClass("clicked")) { 
          hanClicado = true;
        }
      });

      // Si no habían clicado aplicamos los esilos
      if (!hanClicado) {

        if ($(this).hasClass("v")) {
          $(this).css({"background-color" : "#09ff00"});      
        } else {
          $(this).css({"background-color" : "red"});        
        }

        hanClicado = true;
        $(this).removeClass("unclicked").addClass("clicked");
      } 

  });

  $(".preguntas2 > .respuesta2").on("click", function() {

    let hanClicado = false;

    // Miramos todos los hermanos de la pregunta
    $(this).siblings().each(function () {
      if ($(this).hasClass("clicked")) { 
        hanClicado = true;
      }
    });

    // Si no habían clicado aplicamos los esilos
    if (!hanClicado) {

      if ($(this).hasClass("v")) {
        $(this).css({"background-color" : "#09ff00"});      
      } else {
        $(this).css({"background-color" : "red"});        
      }

      hanClicado = true;
      $(this).removeClass("unclicked").addClass("clicked");
    } 

});

$( function() {
  $( ".draggable" ).draggable();
  $( "#droppable" ).droppable({
    drop: function( event, ui ) {
      // Si tiene la class c
      
      if (ui.draggable.hasClass("c")) {
          $( this ).addClass( "verde").find( "p" ).text(ui.draggable.text());
      } else {
        $( this ).addClass( "rojo").find( "p" ).text(ui.draggable.text());
      }
    }

  });
} );



$("#speed").on("change", function() {
  console.log($(this).val());
  var valor = $(this).val();
  if (valor == "sistema"){ 
    $(this).removeClass( "rojo");
    $(this).addClass( "verde");
  } else if(valor =="0") { 
    $(this).removeClass( "verde");
    $(this).removeClass( "rojo");
  }
  else { 
    $(this).removeClass( "verde");
    $(this).addClass( "rojo");
  }
});

$("#speed2").on("change", function() {
  console.log($(this).val());
  var valor = $(this).val();
  if (valor == "normal"){ 
    $(this).removeClass( "rojo");
    $(this).addClass( "verde");
  } else if(valor =="0") { 
    $(this).removeClass( "verde");
    $(this).removeClass( "rojo");
  }
  else { 
    $(this).removeClass( "verde");
    $(this).addClass( "rojo");
  }
});

$("#speed3").on("change", function() {
  console.log($(this).val());
  var valor = $(this).val();
  if (valor == "superusuario"){ 
    $(this).removeClass( "rojo");
    $(this).addClass( "verde");
  } else if(valor =="0") { 
    $(this).removeClass( "verde");
    $(this).removeClass( "rojo");
  }
  else { 
    $(this).removeClass( "verde");
    $(this).addClass( "rojo");
  }
});

});